#!/bin/bash
dpkg -l | grep -qw python3 || sudo apt-get install python3
dpkg -l | grep -qw python3-pip || sudo apt-get install python3-pip
python3 -c "import requests"
reqstatus=$?
if [ "$reqstatus" = "1" ]; then
	pip3 install requests
fi
python3 insp.py &
