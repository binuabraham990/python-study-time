import datetime, time, sys, requests, json
from gi.repository import Notify

def checkTime():
	d1 = datetime.datetime(2016, 9, 23, 16, 59, 59)
	d0 = datetime.datetime.now()
	delta = d1 - d0

	timeformat = 'false'
	no_second = int(delta.total_seconds())
	remain, days, hours, minuts, seconds = 0, 0, 0, 0, 0
	if(no_second != 0):
		days, remain = divmod(no_second, 24*60*60)
		if(remain != 0):
			hours, remain = divmod(remain, 60*60)
		if(remain != 0):
			minuts, seconds = divmod(remain, 60)

		timeformat = '{:02d}:{:02d}:{:02d}:{:02d}'.format(days, hours, minuts, seconds)

	return timeformat

def getQuote():
	t = checkTime()
	if t == 'false':
		sys.exit('Its time')
	else:
		try:
			url = 'http://api.forismatic.com/api/1.0/?method=getQuote&format=json&lang=en'
			response = requests.get(url)
			arryRecieved = json.loads(response.content.decode("utf-8"))
			if len(arryRecieved['quoteText']) <= 120:
				showQuote(arryRecieved['quoteText'], arryRecieved['quoteAuthor'], t)
			else:
				getQuote()
		except:
			pass

def showQuote(quote, author, t):
	summary = quote + "\n" + author
	body = t
	notification = Notify.Notification.new(
		summary,
		body,
	)
	notification.show()

Notify.init("Time To Go")
while 1:
	time.sleep(60)
	getQuote()
