# This time import the GdkPixbuf module
from gi.repository import Notify, GdkPixbuf

Notify.init("Test App")
notification = Notify.Notification.new("Alert!")

# Use GdkPixbuf to create the proper image type
image = GdkPixbuf.Pixbuf.new_from_file("/home/binu/Pictures/1.jpg")

# Use the GdkPixbuf image
notification.set_icon_from_pixbuf(image)
notification.set_image_from_pixbuf(image)

notification.show()
