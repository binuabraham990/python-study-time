import requests
from BeautifulSoup import BeautifulSoup
from gi.repository import Notify



Notify.init("App Name")
#newspaperUrl = "http://www.mathrubhumi.com/latest-news"
newspaperUrl = "http://www.thehindu.com/todays-paper/"
response = requests.get(newspaperUrl)
html = response.content


soup = BeautifulSoup(html)

#result = soup.find_all('b')

news = ""
f = open('news_FRONTPAGE.txt', 'w')
for heading in soup.findAll('div', attrs = { 'class' : 'tpaper' }):
	link  = heading.findNext('a')
	news = news + '\n' + link.text
	f.write(link.text.encode('utf8'))
	f.write('\n')
	f.write(link.get('href'))
	f.write('\n')
#f.write(news.encode('utf8'))
f.close()
Notify.Notification.new(news).show()


def OnClicked(notification):
	notification.close()

