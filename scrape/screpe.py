import requests
from BeautifulSoup import BeautifulSoup
import urllib3

url = 'http://www.djangobook.com/en/2.0/index.html'
response = requests.get(url)
html = response.content

soup = BeautifulSoup(html)
a = soup.find('div', attrs={'id': 'the-django-book'})
no = 0;
for links in a.findAll('a'):
	no = no + 1
	link = links.get('href')
	data_url = "http://www.djangobook.com/en/2.0/" + link
	response_data = requests.get(data_url)
	html_data = response_data.content
	f = open("result/" + str(no) + ".html", 'w')
	f.write(html_data)
	f.close()
	print "downloaded"
print "finished";
