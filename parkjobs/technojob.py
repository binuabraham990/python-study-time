import requests
from bs4 import BeautifulSoup

interestArray = ['PHP', 'Python', 'Web']

url = 'http://www.technopark.org/job-search'
response = requests.get(url)
html = response.content

print("\n" + "=" * 150 + "\n")
soup = BeautifulSoup(html, "lxml")
for link in soup.findAll('a', attrs={'class': 'jobTitleLink'}):
    if any(x in link.text for x in interestArray):
        companynameLink = link.parent.parent;
        print(link.text + " in " + companynameLink.findAll('a')[-1].text
              + " Go to : " + "http://www.technopark.org/" + link.get('href'))
print("\n" + "=" * 150 + "\n")
